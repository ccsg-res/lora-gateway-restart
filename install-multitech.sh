#/bin/sh

if [ `whoami` != root ]; then
    echo "Please run this script as root or using sudo" >&2
    exit
fi

sudo opkg update
sudo opkg install python3-modules
sudo opkg install python3-pip
sudo pip3 install paho-mqtt