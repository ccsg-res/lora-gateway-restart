import paho.mqtt.client as mqtt
import sys
import json
import datetime
import time

host = sys.argv[1]
# print(host)
gateway_id =  sys.argv[2]
gateway_id = gateway_id.lower()
# print(gateway_id)
timeout = sys.argv[3]
stay_connected = True

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    # print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    topic = "gateway/" + gateway_id + "/event/stats"
    sys.stderr.write('sub to topic: %s\r\n' % topic)
    client.subscribe(topic)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    string = msg.payload.decode("utf8")
    json_obj = json.loads(string)
    sys.stderr.write(json.dumps(json_obj))

    t = json_obj['time']
    t=t[:-1] + "+0000" #change the timezone format to be more general and recognizable

    tt = datetime.datetime.strptime(t, "%Y-%m-%dT%H:%M:%S%z")

    print(int(tt.timestamp()))

    global stay_connected
    stay_connected = False
    client.disconnect()
    client.loop_stop()


client = mqtt.Client()

client.on_connect = on_connect
client.on_message = on_message

client.connect(host, port=1883, keepalive=60)

start_time = time.time()
TIMEOUT = 90
try:
    while stay_connected:
        client.loop()
        if time.time() - start_time > TIMEOUT:
            client.loop_stop()
            client.disconnect()
            break
except Exception as e:
    pass
    print(0)